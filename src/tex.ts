import { util } from "./index";

export type RenderTarget = {
    framebuffer: WebGLFramebuffer,
    texture: util.Nullable<WebGLTexture>,
    width: number,
    height: number,
}

export function makeRenderTarget(
    gl: WebGLRenderingContext, width: number, height: number
): RenderTarget
{
    let framebuffer = gl.createFramebuffer()
    if (! util.isDefined(framebuffer)) {
        throw 'Failed to create framebuffer'
    }
    gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer)

    return {
        framebuffer: framebuffer,
        texture: null,
        width: width,
        height: height,
    }
}

export function makeTexture(
    gl: WebGLRenderingContext, width: number, height: number
): WebGLTexture
{
    let texture = gl.createTexture()
    if (! util.isDefined(texture)) {
        throw 'Failed to create texture'
    }

    gl.bindTexture(gl.TEXTURE_2D, texture)
    {
        let internalFormat = gl.RGBA
        let border = 0
        let format = gl.RGBA
        let type = gl.UNSIGNED_BYTE
        let data = null

        gl.texImage2D(
            gl.TEXTURE_2D, 0, internalFormat, width, height, border, format, type, data
        )

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
    }

    return texture
}

export function attachTexture(
    gl: WebGLRenderingContext, target: RenderTarget, texture: WebGLTexture
)
{
    target.texture = texture

    gl.bindFramebuffer(gl.FRAMEBUFFER, target.framebuffer)
    gl.framebufferTexture2D(
        gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0
    )
}

export function useRenderTarget(gl: WebGLRenderingContext, target: RenderTarget) {
    if (! util.isDefined(target.texture)) {
        throw 'Target texture is null'
    }
    gl.bindFramebuffer(gl.FRAMEBUFFER, target.framebuffer)
    gl.viewport(0, 0, target.width, target.height)
}