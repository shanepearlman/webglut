//----------------------------------------------------------------------------------------
//  WebGLUT Random Utility Stuff
//----------------------------------------------------------------------------------------

export type Nullable<T> = T | undefined | null

export function isDefined<T>(value: Nullable<T>): value is T {
    return value !== undefined && value !== null
}

export type Dictionary<T> = {
    [key: string]: Nullable<T>,
}