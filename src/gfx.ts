//----------------------------------------------------------------------------------------
//  WebGLUT Graphics Core Functions
//----------------------------------------------------------------------------------------

import * as util from './util'

//----------------------------------------------------------------------------------------
//  Core graphics types

type AttributeId = string
type AttributeLocationId = string

export type AttributeLocation = number | string  // WebGL annotations are broken!
export type UniformLocation = WebGLUniformLocation | string

export type ProgramInfo = {
    program: WebGLProgram,
    attribLocations: util.Dictionary<AttributeLocation>,
    uniformLocations: util.Dictionary<UniformLocation>,
}

export type Layout = {
    type: number,
    size: number,
    stride: number,
    offset: number,
}

type AttributeData = WebGLBuffer | number[]

export type Attribute = {
    data: AttributeData,
    layout: Layout,
}

export type State = {
    attribCount: number,
    attribOffset: number,
    attributes: util.Dictionary<Attribute>,
}

// DataMap maps AttributeId to AttributeLocationId
export type DataMap = util.Dictionary<AttributeLocationId>

export type Pipeline = {
    programInfo: ProgramInfo,
    state: State,
    dataMap: DataMap,
}

//----------------------------------------------------------------------------------------
//  Render engine utilities

export function makeShaderProgram(
    gl: WebGLRenderingContext, vertSource: string, fragSource: string
)
{
    let vertexShader = loadShader(gl, gl.VERTEX_SHADER, vertSource)
    let fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fragSource)
    let shaderProgram = gl.createProgram()
    if (shaderProgram === null) {
        throw 'Failed to create shader program object'
    }

    gl.attachShader(shaderProgram, vertexShader)
    gl.attachShader(shaderProgram, fragmentShader)
    gl.linkProgram(shaderProgram)
    gl.validateProgram(shaderProgram)
    if (! gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        let infoLog = gl.getShaderInfoLog(shaderProgram)
        gl.deleteProgram(shaderProgram)
        let msg = 'Shader initialization error'
        if (infoLog !== null) {
            msg += ': ' + infoLog
        } else {
            msg += ' (null info log)'
        }
        throw msg
    }

    return shaderProgram
}

function loadShader(gl: WebGLRenderingContext, type: number, source: string) {
    let shader = gl.createShader(type)
    if (shader === null) {
        throw 'Failed to create shader object'
    }

    gl.shaderSource(shader, source)
    gl.compileShader(shader)
    if (! gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        let infoLog = gl.getShaderInfoLog(shader)
        gl.deleteShader(shader)
        let msg = 'Shader compile error'
        if (infoLog !== null) {
            msg += ': ' + infoLog
        } else {
            msg += ' (null info log)'
        }
        throw msg
    }

    return shader
}

export function usePipeline(gl: WebGLRenderingContext, pipeline: Pipeline) {
    const programInfo = pipeline.programInfo
    const state = pipeline.state

    for (let attribId in pipeline.dataMap) {
        if (pipeline.dataMap.hasOwnProperty(attribId)) {
            const attribLocId = pipeline.dataMap[attribId] as AttributeLocationId

            const attrib = state.attributes[attribId]
            if (! util.isDefined(attrib)) {
                throw `No such attribute in pipeline state: ${attribId}`
            }

            let attribLoc = programInfo.attribLocations[attribLocId]
            if (! util.isDefined(attribLoc)) {
                throw `No such attribute location in pipeline program: ${attribLocId}`
            }

            const layout = attrib.layout

            if (attrib.data instanceof Array) {
                let attribBuffer = gl.createBuffer()
                if (attribBuffer === null) {
                    throw `Failed to create buffer for attribute: ${attribId}`
                }
                gl.bindBuffer(gl.ARRAY_BUFFER, attribBuffer)
                let data = new Float32Array(attrib.data as number[])
                gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW)
                attrib.data = attribBuffer
            }

            gl.bindBuffer(gl.ARRAY_BUFFER, attrib.data)

            // attribute location is unresolved?
            if (typeof attribLoc === 'string') {
                let loc = gl.getAttribLocation(programInfo.program, attribLoc)
                if (loc === null) {
                    throw `Failed to get attribute of program: ${attribLoc}`
                }
                programInfo.attribLocations[attribLocId] = loc
                attribLoc = loc
            }

            gl.enableVertexAttribArray(attribLoc)
            gl.vertexAttribPointer(
                attribLoc, layout.size, layout.type, false, layout.stride, layout.offset
            )
        }
    }

    gl.useProgram(programInfo.program)
}

export function render(gl: WebGLRenderingContext, pipeline: Pipeline) {
    let state = pipeline.state
    gl.drawArrays(gl.TRIANGLES, state.attribOffset, state.attribCount)
}